import sqlite3

#Connect to the database.
import cursor as cursor
##List the distinctive major cities located in southern hemisphere ordered by country to the console
# and then write their name, country, and geolocation into a new database table called "Southern cities"
# Find the maximum, minimum and average temperature of Queensland for year 2010 and print this information to the console..
connection = sqlite3.connect('temperature.db')
cursor = connection.cursor()

#Create a new table named SouthernCities.
cursor.execute("drop table if exists SouthernCities")
sql_command1 = """
CREATE TABLE SouthernCities
(
City CHAR (20),
Country CHAR(20),
Latitude CHAR (20),
Longitude CHAR (20)
);
"""
cursor.execute(sql_command1)
print("The Database has been created")

sql_command2 = """
SELECT DISTINCT MajorCity, Country, Latitude, Longitude
FROM GlobalLandTemperaturesByMajorCity
WHERE Latitude LIKE '%S'
ORDER BY Country;
"""
cursor.execute(sql_command2)
result = cursor.fetchall()

for i in result:
    MajorCity = i[0]
    Country = i[1]
    Latitude = i[2]
    Longitude = i[3]
    sql_command = """INSERT INTO SouthernCities(city, country, latitude, longitude)
    VALUES('"""+ MajorCity +"',\""+ Country + "\",'"+ Latitude+"','"+Longitude+"')"

sql_command1 = """
SELECT MAX(AverageTemperature), MIN(AverageTemperature), AVG(AverageTemperature)
FROM GlobalLandTemperaturesByState
WHERE DATE1 LIKE '2000%' and state = 'Queensland';
"""
sql_command2 = """
SELECT MAX(AverageTemperature), MIN(AverageTemperature), AVG(AverageTemperature)
FROM GlobalLandTemperaturesByState
WHERE DATE1 LIKE '2000%';
"""
cursor.execute(sql_command1)
result = cursor.fetchall()
for i in result:
    print("Queensland")
    print (i)
cursor.execute(sql_command2)
result = cursor.fetchall()
for i in result:
    print("Worldwide")
    print(i)