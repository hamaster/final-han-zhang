import openpyxl
import sqlite3
###first part
connection = sqlite3.connect('temperature.db')
cursor = connection.cursor()
cursor.execute("""DROP TABLE IF EXISTS GlobalLandTempurtareByCountry""")
cursor.execute("""CREATE TABLE GlobalLandTempurtareByCountry
(DATE DATE,
AverageTemperature FLOAT ,
AverageTemperatureUncertainty FLOAT,
Country CHAR(20))""")
#create a data base for workbook1
wb1 = openpyxl.load_workbook('GlobalLandTemperaturesByCountry.xlsx')
ws1 = wb1.active
#read the 1st excel
m = ws1.max_row
## for reducing the time, cell would only loop in 1 col
###
for row in ws1.iter_rows(min_row = 2, max_row = m, max_col= 1):
    for cell in row:
        n = cell.row
        DATA = ws1["A"+str(n)].value
        AverageTemperature = ws1["B"+str(n)].value
        AverageTemperatureUncertainty = ws1["C"+str(n)].value
        Country = ws1["D"+str(n)].value
        sql_command2 = """
        INSERT INTO GlobalLandTempurtareByCountry(DATE,AverageTemperature,AverageTemperatureUncertainty,Country)
        VALUES('""" +str(DATA)+ "','"+ str(AverageTemperature)+"','"+ str(AverageTemperatureUncertainty)+"',\"" + Country+ "\")"
    cursor.execute(sql_command2)
    ### SQL and Python are so different.Without \' and \" SQL would do nothing but errors.
connection.commit()
print("1st table done")
###2nd part
cursor.execute("""DROP TABLE IF EXISTS GlobalLandTemperaturesByMajorCity""")
cursor.execute("""CREATE TABLE GlobalLandTemperaturesByMajorCity
(DATE CHAR(20),
AverageTemperature FLOAT ,
AverageTemperatureUncertainty FLOAT,
MajorCity MESSAGE_TEXT,
Country CHAR(20),
Latitude CHAR(20),
Longitude CHAR(20))""")
#create a data base for workbook2
wb2 = openpyxl.load_workbook('GlobalLandTemperaturesByMajorCity.xlsx')
ws2 = wb2.active
#read the excel2
m = ws2.max_row
for row in ws2.iter_rows(min_row = 2, max_row = m, max_col=1 ):
    for cell in row:
        n = cell.row
        DATA = ws2["A"+str(n)].value
        AverageTemperature = ws2["B"+str(n)].value
        AverageTemperatureUncertainty = ws2["C"+str(n)].value
        MajorCity = ws2["D"+str(n)].value
        Country = ws2["E"+str(n)].value
        Latitude = ws2["F"+str(n)].value
        Longitude = ws2["G"+str(n)].value
        sql_command4 = """
            INSERT INTO GlobalLandTemperaturesByMajorCity(DATE, AverageTemperature,AverageTemperatureUncertainty, MajorCity, Country, Latitude, Longitude)
            VALUES('""" + str(DATA) + "','" + str(AverageTemperature) + "','" + str(
            AverageTemperatureUncertainty) + "','" + MajorCity + "',\"" + Country + "\",'" + Latitude + "','" + Longitude + "')"

        cursor.execute(sql_command4)
connection.commit()
print("2nd table has been done")

####3rd part
cursor.execute("""DROP TABLE IF EXISTS GlobalLandTemperaturesByState""")
cursor.execute("""CREATE TABLE GlobalLandTemperaturesByState
(DATE CHAR(20),
AverageTemperature FLOAT ,
AverageTemperatureUncertainty FLOAT,
State CHAR(20),
 Country CHAR(20))""")
#create a data base for workbook3
wb3 = openpyxl.load_workbook('GlobalLandTemperaturesByState.xlsx')
ws3 = wb3.active
#read the excel3
m = ws3.max_row
print("Running 3rd table")
for row in ws3.iter_rows(min_row = 2, max_row = m,max_col = 1):
    for cell in row:
        n = cell.row
        DATA = ws3["A"+str(n)].value
        AverageTemperature = ws3["B"+str(n)].value
        AverageTemperatureUncertainty = ws3["C"+str(n)].value
        State = ws3["D"+str(n)].value
        Country = ws3["E"+str(n)].value
        sql_command2 = """
                INSERT INTO GlobalLandTemperaturesByState(DATE,AverageTemperature,AverageTemperatureUncertainty,State,Country)
                VALUES('""" + str(DATA) + "','" + str(AverageTemperature) + "','" + str(
                AverageTemperatureUncertainty) + "',\""+State+"\",\" "+ Country + "\")"
        cursor.execute(sql_command2)
connection.commit()
print("ALL DONE")
##after all, i can find any information that we have in 3 excel files
#This code ran such a long time.I've never wanted a super computer badly like today.