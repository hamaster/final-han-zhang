import openpyxl
import sqlite3
import matplotlib.pyplot as plt
#Open the World Temperature workbook.
#Create another sheet called Comparison.
#Calculate the mean temperature of Australian states for each year (using the temperature by state table in your database).
#Calculate the mean temperature of Australia for each year (using the temperature by country table in your database).
#Calculate their differences between each state and the national data for each year.
#Use MatPlotLib to plot the difference across years. You need to include this plot in your report in Task 5 below.
#Write the data into the sheet.
wb= openpyxl.load_workbook("World Temperature.xlsx")
ws = wb.active
ws2 = wb.create_sheet(title = "Comparison", index=1)
con = sqlite3.connect("temperature.db")
cur = con.cursor()
# All files are read
#should pull out data where Country = australia
cur.execute("""drop table if exists AvgTempByState""")
sql_command = """
CREATE TABLE AvgTempByState
(
date TEXT,
temp REAL,
state TEXT
);
"""
cur.execute(sql_command)
cur.execute("""DROP TABLE if EXISTS Australia""")
cur.execute("""CREATE TABLE Australia
( DATE1 date, Temperature FLOAT, State CHAR(20));""")
#set up a new table

command = """
SELECT DATE1, AverageTemperature, State 
FROM GlobalLandTemperaturesByState 
WHERE Country="Australia"
ORDER BY State, DATE1;"""
cur.execute(command)
result = cur.fetchall()
print(result)
for i in result:
    DATE = i[0]
    Temperature = i[1]
    State = i[2]
    cur.execute("""INSERT INTO Australia(DATE1, Temperature,State)VALUES ('"""+str(DATE)+"','"+
            str(Temperature)+"',\'"+State+"\")")

