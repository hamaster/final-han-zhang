import openpyxl
import sqlite3
#0Query the database and calculate the yearly mean temperature of each city in China.
#  Note some data may be missing.
#Write the relevant data into the worksheet you created and save it
wb = openpyxl.load_workbook('World Temperature.xlsx')
wb.create_sheet(index=0,title ="Temperature by city")
ws = wb.active
connection = sqlite3.connect('temperature.db')
cursor = connection.cursor()
cursor.execute("""drop table if exists TempByChineseCity""")
cursor.execute("""
CREATE TABLE TempByChineseCity
(
DATE DATE,
Temp FLOAT ,
City CHAR(20)
);
""")
cursor.execute("""SELECT DATE,AverageTemperature,MajorCity FROM GlobalLandTemperaturesByMajorCity
WHERE Country = 'China' ORDER BY MajorCity,DATE ;""")
# Looking for every data from china rows
result = cursor.fetchall()
for i in result:
    DATE = i[0]
    Temp= i[1]
    City = i[2]
    command1 ="""INSERT INTO TempByChineseCity(DATE,Temp,City)
     VALUES ("{DATE}","{Temp}","{City}")"""
    cursor.execute(command1.format(DATE =DATE,Temp =Temp, City =City))
#insert info into china table
#dont know how to write them in the form of SQL RP bucause they are not xxx\',yyy\".
# That is why I use format funcation instead of
cursor.execute("""
SELECT SUM(Temp) AS whole, COUNT(Temp) AS count, City, strftime('%Y', DATE) AS year
FROM TempByChineseCity
GROUP BY city, year; 
""")
## I think this task is about Chinese data. so I didn't write all data in world wide into this excel
result =cursor.fetchall()
ws["A1"].value = "AverageTemperature"
ws["B1"].value = 'City'
ws["C1"].value = 'Year'
for i in range(len(result)):
    ws.cell(row=i+2, column=1).value = result[i][0]/result[i][1]
    ws.cell(row=i+2, column=2).value = result[i][2]
    ws.cell(row=i+2, column=3).value = result[i][3]

for i in range(1,len(result)+1):
    print('It is ' + str(ws.cell(row=i,column=1).value) + 'C in ' + str(ws.cell(row=i,column=2).value) + ' at the year ' + str(ws.cell(row=i,column=3).value) + '')
wb.save('World Temperature.xlsx')